package Controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rúben Terra
 */
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import java.sql.*;

public class DBconnect {

    private Connection con;
    private Statement stmt;

    public DBconnect() {

    }

    public void criar_ligacao() {
       
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            this.con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=bd_rent_a_car;user=admin;password=123");
            this.stmt = this.con.createStatement();
        } catch (Exception e) {
            System.out.println(e);
            gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a fazer a ligação a base de dados");
        }
          
    }

    public void fechar_conecao() {

        try {
            this.stmt.close();
            this.con.close();
        } catch (Exception e) {
            gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a fechar a ligação a base de dados");
        }

    }

    public ResultSet execQueryRS(String query) {

        ResultSet rs = null;
        try {
            rs = stmt.executeQuery(query);
        } catch (Exception e) {
            gerarMsgBox(AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a executar uma consulta a base de dados");
        }
        return rs;
        
    }
    
    public boolean execQueryBoolean(String query) {

        boolean result = false;
        try {
            stmt.executeUpdate(query);
            result = true;
        } catch (Exception e) {
            gerarMsgBox(AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a executar uma operação a base de dados");
        }
        return result;

    }

    public void gerarMsgBox(AlertType tipoAlert, String titulo, String textoPrincipal, String textoSecundario) {

        Alert alert = new Alert(tipoAlert);
        alert.setTitle(titulo);
        alert.setHeaderText(textoPrincipal);
        alert.setContentText(textoSecundario);

        alert.showAndWait();

    }

    public boolean gerarPerguntaBox(AlertType tipoAlert, String titulo, String textoPrincipal, String pergunta, String btn1, String btn2) {

        Alert alert = new Alert(tipoAlert);
        alert.setTitle(titulo);
        alert.setHeaderText(textoPrincipal);
        alert.setContentText(pergunta);

        ButtonType button1 = new ButtonType(btn1);
        ButtonType button2 = new ButtonType(btn2);

        alert.getButtonTypes().setAll(button1, button2);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == button1) {
            return true;
        } else {
            return false;
        }
}


}


