package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class MenuController implements Initializable {  ///classe para operar a pagina do menu

    @FXML
    private JFXButton bt_menufunc;

    @FXML
    private JFXButton menucarros;

    @FXML
    private JFXButton menualuguer;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO Auto-generated method stub

    }
 @FXML
    void bt_menufunc_ac(ActionEvent event) throws IOException { ///abrir pagina do menu  funcionario

        Stage menu1 = new Stage();
        Parent root1 = FXMLLoader.load(getClass().getResource("/FXML/MenuFunc.fxml"));
        Scene scene = new Scene(root1);
        menu1.setScene(scene);
        menu1.setResizable(false);
        menu1.show();
    }
 @FXML
    void menualuguer_ac(ActionEvent event) throws IOException { ///abrir pagina do menu aluguer
        Stage menu219 = new Stage();
        Parent root219 = FXMLLoader.load(getClass().getResource("/FXML/MenuAlug.fxml"));
        Scene scene = new Scene(root219);
        menu219.setScene(scene);
        menu219.setResizable(false);
        menu219.show();
    }
 @FXML
    void menucarros_ac(ActionEvent event) throws IOException { ///abrir pagina do menu  carros

        Stage menu9 = new Stage();
        Parent root9 = FXMLLoader.load(getClass().getResource("/FXML/MenuCarros.fxml"));
        Scene scene = new Scene(root9);
        menu9.setScene(scene);
        menu9.setResizable(false);
        menu9.show();
    }

    

}
