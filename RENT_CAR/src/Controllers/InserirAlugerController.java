/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author david
 */
public class InserirAlugerController implements Initializable {

    @FXML
    private CheckBox sim;

    @FXML
    private TextField tx_nome_cliente;

    @FXML
    private TextField tx_n_contribuinte_cliente;

    @FXML
    private TextField dp_data_inicial;

    @FXML
    private TextField dp_data_final;

    @FXML
    private TextField tx_valor_pago_dia;

    @FXML
    private TextField tx_nome_condutor;

    @FXML
    private TextField tx_contribuinte_condutor;

    @FXML
    private Button bt_criar_alug;

    @FXML
    private TextField tx_valor_pago;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void bt_criar_alug_ac(ActionEvent event) {
        if (tx_nome_cliente.getText() == null || tx_n_contribuinte_cliente.getText() == null || tx_nome_condutor.getText() == null || tx_contribuinte_condutor.getText() == null || dp_data_inicial.getText() == null || dp_data_final.getText() == null || tx_valor_pago_dia.getText() == null || tx_valor_pago.getText() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            String content = "Preencha os dados todos!";
            alert.setContentText(content);
            alert.showAndWait();
        } else {

            int valor = Integer.parseInt(tx_valor_pago.getText());
            int data_inicial = Integer.parseInt(dp_data_inicial.getText());
            int data_final = Integer.parseInt(dp_data_final.getText());
            String condutor = tx_nome_condutor.getText();
            int entrega_domicilio = Integer.parseInt(sim.getText());

            Aluguer aluguer = new Aluguer(valor, data_inicial, data_final, condutor, entrega_domicilio);
            aluguer.criarAluguer();

        }
    }

}
