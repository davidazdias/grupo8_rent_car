package Controllers;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import javafx.scene.control.Alert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rúben Terra
 */
public class Carro {

    protected int id_veiculo;
    protected String marca;
    protected String modelo;
    protected LocalDate data_revisao;
    protected String matricula;
    protected int disponibilidade;
    protected int preco;

    public Carro(int id_veiculo) {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        ResultSet rs = query.execQueryRS("SELECT * FROM Veiculo WHERE Veiculo.id_veiculo = " + id_veiculo);

        try {
            while (rs.next()) {
                this.id_veiculo = rs.getInt("id_veiculo");
                this.marca = rs.getString("marca");
                this.modelo = rs.getString("modelo");
                this.data_revisao = rs.getDate("data_revisao").toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                this.matricula = rs.getString("matricula");
                this.disponibilidade = rs.getInt("disponibilidade");
                this.preco = rs.getInt("preco");
            }
            rs.close();
        } catch (Exception e) {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a processar os dados da base de dados do veiculo");
        }
        query.fechar_conecao();
    }

    public Carro(int id_veiculo, String marca, String modelo, String matricula, LocalDate data_revisao, int disponibilidade, int preco) {
        this.id_veiculo = id_veiculo;
        this.marca = marca;
        this.modelo = modelo;
        this.data_revisao = data_revisao;
        this.matricula = matricula;
        this.disponibilidade = disponibilidade;
        this.preco = preco;

    }

    public Carro(String marca, String modelo, String matricula, int disponibilidade, String password, int preco, int tipo) {

        this.marca = marca;
        this.modelo = modelo;
       // this.data_revisao = data_revisao;
        this.matricula = matricula;
        this.disponibilidade = disponibilidade;
        this.preco = preco;

    }

   

    Carro(String marca, String modelo, String matricula, int preco) {
        this.marca = marca;
        this.modelo = modelo;
       // this.data_revisao = data_revisao;
        this.matricula = matricula;
        //this.disponibilidade = disponibilidade;
        this.preco = preco;
    }

    
    
    /**
     * @return the id_veiculo
     */
    public int getid_veiculo() {
        return id_veiculo;
    }

    /**
     * @param id_veiculo the id_veiculo to set
     */
    public void setid_veiculo(int id_veiculo) {
        this.id_veiculo = id_veiculo;
    }

    /**
     * @return the marca
     */
    public String getmarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setmarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the modelo
     */
    public String getmodelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setmodelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return the data_revisao
     */
    public LocalDate getdata_revisao() {
        return data_revisao;
    }

    /**
     * @param data_revisao the data_revisao to set
     */
    public void setdata_revisao(LocalDate data_revisao) {
        this.data_revisao = data_revisao;
    }

    /**
     * @return the matricula
     */
    public String getmatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setmatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the preco
     */
    public int getpreco() {
        return preco;
    }

    /**
     * @param preco the preco to set
     */
    public void setpreco(int preco) {
        this.preco = preco;
    }

    /**
     * @return the tipo
     */
    public void criarCarro() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("insert into Veiculo (marca, modelo, matricula, disponibilidade, data_revisao, preco) values ('" + this.marca + "', '" + this.modelo + "', '" + this.matricula + "', '" + this.disponibilidade + "', '" + this.data_revisao + "', '" + this.preco + "')")) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do utilizador foram inseridos com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel inserir os dados do utilizador com sucesso");
        }

        ResultSet rs = query.execQueryRS("SELECT id_veiculo FROM Veiculo order by id_veiculo desc limit 1");
        try {
            rs.next();
            this.id_veiculo = rs.getInt("id_veiculo");
        } catch (Exception e) {

        }
        query.fechar_conecao();
    }

    public void updateCarro() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("update Veiculo set marca = '" + this.marca + "', modelo = '" + this.modelo + "', matricula = '" + this.matricula + "', disponibilidade = " + this.disponibilidade + ", data_revisao = '" + this.data_revisao + "', preco = " + this.preco + " where id = " + this.id_veiculo)) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do veiculo foram alterados com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel alterar os dados do veiculo com sucesso");
        }
        query.fechar_conecao();
    }
}
