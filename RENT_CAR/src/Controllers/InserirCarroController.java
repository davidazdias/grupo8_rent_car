/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author david
 */
public class InserirCarroController implements Initializable {

    private JFXButton voltar_inserir_carro;
    @FXML
    private TextField tx_marca;
    @FXML
    private TextField tx_modelo;
    @FXML
    private TextField tx_matricula;
    @FXML
    private TextField tx_preco;
    @FXML
    private TableColumn<?, ?> cl_marca;
    @FXML
    private TableColumn<?, ?> cl_modelo;
    @FXML
    private TableColumn<?, ?> cl_matricula;
    @FXML
    private TableColumn<?, ?> cl_disponibilidade;
    @FXML
    private TableColumn<?, ?> cl_data_revisao;
    @FXML
    private TableColumn<?, ?> cl_preco;
    @FXML
    private TextField tx_disponibilidade;
    @FXML
    private DatePicker dp_data_revisao;
    @FXML
    private Button btn_criar;

    /**
     * Initializes the controller class.
     */
 
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    private void voltar_inserir_carro_ac(ActionEvent event) throws IOException {
        Stage menu1 = new Stage();
        Parent root1 = FXMLLoader.load(getClass().getResource("/FXML/MenuCarro.fxml"));
        Scene scene = new Scene(root1);
        menu1.setScene(scene);
        menu1.setResizable(false);
        voltar_inserir_carro.getScene().getWindow().hide();
        menu1.show();
    }

    @FXML
    private void tx_marca_ac(ActionEvent event) {
    }

    @FXML
    private void tx_modelo_ac(ActionEvent event) {
    }

    @FXML
    private void tx_matricula_ac(ActionEvent event) {
    }

    @FXML
    private void tx_disponibilidade_ac(ActionEvent event) {
    }

    @FXML
    private void tx_preco_ac(ActionEvent event) {
    }

    @FXML
    private void dp_revisao_ac(ActionEvent event) {
    }

    @FXML
    private void btn_criar_ac(ActionEvent event) {
        
        if (tx_marca.getText() == null || tx_modelo.getText() == null || tx_matricula.getText() == null || tx_preco.getText() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            String content = "Preencha os dados todos!";
          alert.setContentText(content);
           alert.showAndWait();
       } else {
           String marca = tx_marca.getText();
           String modelo = tx_modelo.getText(); //dp_data_revisao.getValue();
           String matricula = tx_matricula.getText();
           // String data_revisao = tx_data_revisao.getText();
            int preco = Integer.parseInt(tx_preco.getText());

            Carro carro = new Carro(marca, modelo, matricula, preco);
            carro.criarCarro();
    
        }
  }
        
        
    }
    

