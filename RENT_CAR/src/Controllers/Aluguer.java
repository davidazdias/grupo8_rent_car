package Controllers;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import javafx.scene.control.Alert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author David Dias
 */
public class Aluguer {

    protected int id_contrato;
    protected int id_cliente;
    protected int id_veiculo;
    protected int valor;
    protected int data_inicial;
    protected int data_final;
    protected String condutor;
    protected int entrega_domicilio;
    protected int id_custo;

    public Aluguer(int id_contrato) {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        ResultSet rs = query.execQueryRS("SELECT * FROM Contrato_aluguer WHERE Veiculo.id_contrato = " + id_contrato);

        try {
            while (rs.next()) {
                this.id_contrato = rs.getInt("id_contrato");
                this.id_cliente = rs.getInt("id_cliente");
                this.id_veiculo = rs.getInt("id_veiculo");
                this.valor = rs.getInt("valor");
                this.data_inicial = rs.getInt("data_inicial");
                this.data_final = rs.getInt("data_final");
                this.condutor = rs.getString("condutor");
                this.entrega_domicilio = rs.getInt("entrega_domicilio");
                this.id_custo = rs.getInt("id_custo");
            }
            rs.close();
        } catch (Exception e) {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a processar os dados da base de dados do Contrato Aluguer");
        }
        query.fechar_conecao();
    }

    public Aluguer(int id_contrato, int id_cliente, int id_veiculo, int valor, int data_inicial, int data_final, String condutor, int entrega_domicilio, int id_custo) {
        this.id_contrato = id_contrato;
        this.id_cliente = id_cliente;
        this.id_veiculo = id_veiculo;
        this.valor = valor;
        this.data_inicial = data_inicial;
        this.data_final = data_final;
        this.condutor = condutor;
        this.entrega_domicilio = entrega_domicilio;
        this.id_custo = id_custo;
    }

    Aluguer(int valor, int data_inicial, int data_final, String condutor, int entrega_domicilio) {

        this.valor = valor;
        this.data_inicial = data_inicial;
        this.data_final = data_final;
        this.condutor = condutor;
        this.entrega_domicilio = entrega_domicilio;

    }


    /**
     * @return the id_contrato
     */
    public int getid_contrato() {
        return id_contrato;
    }

    /**
     * @param id_contrato the id_contrato to set
     */
    public void setid_contrato(int id_contrato) {
        this.id_contrato = id_contrato;
    }

    /**
     * @return the is_cliente
     */
    public int getid_cliente() {
        return id_cliente;
    }

    /**
     * @param is_cliente the is_cliente to set
     */
    public void setid_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the id_veiculo
     */
    public int getid_veiculo() {
        return id_veiculo;
    }

    /**
     * @param id_veiculo the id_veiculo to set
     */
    public void setid_veiculo(int id_veiculo) {
        this.id_veiculo = id_veiculo;
    }

    /**
     * @return the data_inicial
     */
    public int getdata_inicial(int data_inicial) {
        return data_inicial;
    }

    /**
     * @param data_inicial the data_inicial to set
     */
    public void setdata_inicial(int data_inicial) {
        this.data_inicial = data_inicial;
    }

    /**
     * @return the data_final
     */
    public int getdata_final() {
        return data_final;
    }

    /**
     * @param data_final the data_final to set
     */
    public void setdata_final(int data_final) {
        this.data_final = data_final;
    }

    /**
     * @return the condutor
     */
    public String getcondutor() {
        return condutor;
    }

    /**
     * @param condutor the condutor to set
     */
    public void setcondutor(String condutor) {
        this.condutor = condutor;
    }

    public int getentrega_domicilio() {
        return entrega_domicilio;
    }

    /**
     * @param condutor the condutor to set
     */
    public void setentrega_domicilio(int entrega_domicilio) {
        this.entrega_domicilio = entrega_domicilio;
    }

    public int get_domicilio() {
        return entrega_domicilio;
    }

    public void setid_custo(int id_custo) {
        this.id_custo = id_custo;
    }

    public int getid_custo() {
        return id_custo;
    }

    /**
     * @return the tipo
     */
    public void criarAluguer() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("insert into Contrato_aluguer (id_contrato ,id_cliente, id_veiculo, valor, data_inicial, data_final, condutor, entrega_domicilio, id_custo) values ('" + this.id_contrato + "','" + this.id_cliente + "', '" + this.id_veiculo + "', '" + this.valor + "', '" + this.data_inicial + "', '" + this.data_final + "', '" + this.condutor + "', '" + this.entrega_domicilio + "' '" + this.id_custo + "')")) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do utilizador foram inseridos com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel inserir os dados do utilizador com sucesso");
        }

        ResultSet rs = query.execQueryRS("SELECT id_contrato FROM Veiculo order by id_contrato desc limit 1");
        try {
            rs.next();
            this.id_contrato = rs.getInt("id_contrato");
        } catch (Exception e) {

        }
        query.fechar_conecao();
    }

    public void updateAluguer() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("update Contrato_aluguer set id_contrato = '" + this.id_contrato + "','" + this.id_cliente + "', '" + this.id_veiculo + "', '" + this.valor + "', '" + this.data_inicial + "', '" + this.data_final + "', '" + this.condutor + "', '" + this.entrega_domicilio + "' '" + this.id_custo + "')")) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do veiculo foram alterados com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel alterar os dados do contrato com sucesso");
        }
        query.fechar_conecao();
    }
}
