package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LoginController implements Initializable {

    @FXML
    private JFXTextField username;

    @FXML
    private JFXButton login;

    @FXML
    private JFXPasswordField passe;

    @FXML
    private ImageView loading;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loading.setVisible(false);
        username.setStyle("-fx-text-fill: #ffffff;");
        passe.setStyle("-fx-text-fill: #ffffff;");

    }

    @FXML
    public void loginAction(ActionEvent e) throws IOException {

        Stage menu = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/FXML/Menu.fxml"));
        Scene scene = new Scene(root);
        menu.setScene(scene);
        menu.setResizable(false);

        loading.setVisible(true);
        PauseTransition pt = new PauseTransition();
        pt.setDuration(Duration.seconds(3));

        pt.setOnFinished(ev -> {
            System.out.print("Login com Sucesso!");
            login.getScene().getWindow().hide();
            menu.show();

        });
        pt.play();

    }

}
