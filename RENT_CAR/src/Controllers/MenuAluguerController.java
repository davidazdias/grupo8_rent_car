package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MenuAluguerController implements Initializable { //classe para operar as janelas do menu aluguer

    @FXML
    private JFXButton voltaralug;
    @FXML
    private JFXButton inserir_alug;
    private JFXButton voltar_inserir_aluguer;

    void voltar_inserir_aluguer_ac(ActionEvent event) throws IOException {
        Stage menu19321 = new Stage();
        Parent root19321 = FXMLLoader.load(getClass().getResource("/FXML/MenuAlug.fxml"));
        Scene scene = new Scene(root19321);
        menu19321.setScene(scene);
        menu19321.setResizable(false);
        voltar_inserir_aluguer.getScene().getWindow().hide();
        menu19321.show();
    }

    @FXML
    void inserir_alug_ac(ActionEvent event) throws IOException {
        Stage menu9321 = new Stage();
        Parent root9321 = FXMLLoader.load(getClass().getResource("/FXML/InserirAluguer.fxml"));
        Scene scene = new Scene(root9321);
        menu9321.setScene(scene);
        menu9321.setResizable(false);
        menu9321.show();
    }

    @FXML
    void voltaralug_ac(ActionEvent event) throws IOException { ///botao voltar para o menu principal 
        Stage menu932 = new Stage();
        Parent root932 = FXMLLoader.load(getClass().getResource("/FXML/Menu.fxml"));
        Scene scene = new Scene(root932);
        menu932.setScene(scene);
        menu932.setResizable(false);
        voltaralug.getScene().getWindow().hide();
        menu932.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO Auto-generated method stub

    }

}
