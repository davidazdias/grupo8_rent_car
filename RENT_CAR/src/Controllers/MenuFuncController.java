/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author david
 */
public class MenuFuncController implements Initializable {

    @FXML
    private Button bt_inserir_func;
    @FXML
    private Button alterar_func;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void bt_inserir_func_ac(ActionEvent event) throws IOException {
             Stage menu1012 = new Stage();
        Parent root1012 = FXMLLoader.load(getClass().getResource("/FXML/InserirFunc.fxml"));
        Scene scene = new Scene(root1012);
        menu1012.setScene(scene);
        menu1012.setResizable(false);
        menu1012.show();
    }

    @FXML
    private void alterar_func_ac(ActionEvent event) throws IOException {
             Stage menu1012 = new Stage();
        Parent root1012 = FXMLLoader.load(getClass().getResource("/FXML/AlterarFunc.fxml"));
        Scene scene = new Scene(root1012);
        menu1012.setScene(scene);
        menu1012.setResizable(false);
        menu1012.show();
    }
    
}
