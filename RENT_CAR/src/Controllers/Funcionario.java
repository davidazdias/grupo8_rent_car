package Controllers;

import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.ZoneId;
import javafx.scene.control.Alert;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rúben Terra
 */
public class Funcionario {

    protected int id_funcionario;
    protected String nome;
    protected String palavra_passe;
    protected int contacto;
    protected String email;

    public Funcionario(int id_funcionario) {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        ResultSet rs = query.execQueryRS("SELECT * FROM Funcionario WHERE Funcionario.id_funcionario = " + id_funcionario);

        try {
            while (rs.next()) {
                this.id_funcionario = rs.getInt("id_funcionario");
                this.nome = rs.getString("nome");
                this.palavra_passe = rs.getString("palavra_passe");
                this.contacto = rs.getInt("contacto");
                this.email = rs.getString("email");
            }
            rs.close();
        } catch (Exception e) {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Ocorreu um erro a processar os dados da base de dados do funcionario");
        }
        query.fechar_conecao();
    }

  

    Funcionario(String nome, String palavra_passe, int contacto, String email) {
        this.nome = nome;
        this.palavra_passe = palavra_passe;
        this.contacto = contacto;
        this.email = email;
    
    }

    
    
    /**
     * @return the id_funcionario
     */
    public int getid_funcionario() {
        return id_funcionario;
    }

    /**
     * @param id_funcionario the id_funcionario to set
     */
    public void setid_funcionario(int id_funcionario) {
        this.id_funcionario = id_funcionario;
    }

    /**
     * @return the nome
     */
    public String getnome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setnome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the palavra_passe
     */
    public String getpalavra_passe() {
        return palavra_passe;
    }

    /**
     * @param palavra_passe the palavra_passe to set
     */
    public void setpalavra_passe(String palavra_passe) {
        this.palavra_passe = palavra_passe;
    }

  

  

    /**
     * @return the contacto
     */
    public int getcontacto() {
        return contacto;
    }

    /**
     * @param contacto the contacto to set
     */
    public void setcontacto(int contacto) {
        this.contacto = contacto;
    }


    /**
     * @return the tipo
     */
    public void criarFuncionario() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("insert into Funcionario (nome, palavra_passe, contacto, email) values ('" + this.nome + "', '" + this.palavra_passe + "', '" + this.contacto + "', '" + this.email + "')")) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do utilizador foram inseridos com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel inserir os dados do funcionario com sucesso");
        }

        ResultSet rs = query.execQueryRS("SELECT id_funcionario FROM Funcionario order by id_funcionario desc limit 1");
        try {
            rs.next();
            this.id_funcionario = rs.getInt("id_funcionario");
        } catch (Exception e) {

        }
        query.fechar_conecao();
    }

    public void updateFuncionario() {
        DBconnect query = new DBconnect();
        query.criar_ligacao();
        if (query.execQueryBoolean("update Funcionario set nome = '" + this.nome + "', palavra_passe = '" + this.palavra_passe + "', contacto = '" + this.contacto + "', email = " + this.email +  " where id = " + this.id_funcionario)) {
            query.gerarMsgBox(Alert.AlertType.INFORMATION, "Inserção de dados", "Base de dados", "Os dados do funcionario foram alterados com sucesso");
        } else {
            query.gerarMsgBox(Alert.AlertType.ERROR, "Erro", "Base de dados", "Não foi possivel alterar os dados do funcionario com sucesso");
        }
        query.fechar_conecao();
    }
}
