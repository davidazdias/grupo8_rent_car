/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;

/**
 * FXML Controller class
 *
 * @author david
 */
public class AlterarCarroController implements Initializable {

    @FXML
    private TableColumn<?, ?> cl_nome;
    @FXML
    private TableColumn<?, ?> cl_email;
    @FXML
    private TableColumn<?, ?> cl_data_de_nascimento;
    @FXML
    private TableColumn<?, ?> cl_password;
    @FXML
    private TableColumn<?, ?> cl_contacto;
    @FXML
    private TableColumn<?, ?> cl_contacto1;
    @FXML
    private JFXButton voltar_alterar_carro;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void voltar_alterar_carro_ac(ActionEvent event) {
    }
    
}
