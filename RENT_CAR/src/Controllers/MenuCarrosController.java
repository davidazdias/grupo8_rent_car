package Controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MenuCarrosController implements Initializable {  ///classe para operar as janelas do menu carro

    @FXML
    private JFXButton voltarmenu;

    @FXML
    private JFXButton inserir_carros;

    @FXML
    private JFXButton alterar_remover;

    private JFXButton voltar_alterar_carro;

    private JFXButton voltar_inserir_carro;

    void voltar_inserir_carro_ac(ActionEvent event) throws IOException {
        Stage menu1220 = new Stage();
        Parent root1220 = FXMLLoader.load(getClass().getResource("/FXML/MenuCarros.fxml"));
        Scene scene = new Scene(root1220);
        menu1220.setScene(scene);
        menu1220.setResizable(false);
        voltar_inserir_carro.getScene().getWindow().hide();
        menu1220.show();
    }

    void voltar_alterar_carro_ac(ActionEvent event) throws IOException {
        Stage menu10 = new Stage();
        Parent root10 = FXMLLoader.load(getClass().getResource("/FXML/MenuCarros.fxml"));
        Scene scene = new Scene(root10);
        menu10.setScene(scene);
        menu10.setResizable(false);
        voltar_alterar_carro.getScene().getWindow().hide();
        menu10.show();
    }

    @FXML
    void alterar_remover_ac(ActionEvent event) throws IOException {
        Stage menu1012 = new Stage();
        Parent root1012 = FXMLLoader.load(getClass().getResource("/FXML/AlterarCarro.fxml"));
        Scene scene = new Scene(root1012);
        menu1012.setScene(scene);
        menu1012.setResizable(false);
        alterar_remover.getScene().getWindow().hide();
        menu1012.show();
    }

    @FXML
    void voltarmenu_ac(ActionEvent event) throws IOException { //Botao voltar do menu carros para o menu principal
        Stage menu10 = new Stage();
        Parent root10 = FXMLLoader.load(getClass().getResource("/FXML/Menu.fxml"));
        Scene scene = new Scene(root10);
        menu10.setScene(scene);
        menu10.setResizable(false);
        voltarmenu.getScene().getWindow().hide();
        menu10.show();
    }

    @FXML
    void inserir_carro_ac(ActionEvent event) throws IOException {
        Stage menu1021 = new Stage();
        Parent root1021 = FXMLLoader.load(getClass().getResource("/FXML/InserirCarro.fxml"));
        Scene scene = new Scene(root1021);
        menu1021.setScene(scene);
        menu1021.setResizable(false);
        menu1021.show();
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub

    }

}
