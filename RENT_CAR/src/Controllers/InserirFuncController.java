/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author david
 */
public class InserirFuncController implements Initializable {

    @FXML
    private TextField nome_utilizador;
    @FXML
    private TextField passe_utilizador;

    @FXML
    private Button bt_criar;
    @FXML
    private TextField tx_email;
    @FXML
    private TextField tx_contacto;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }


    @FXML
    private void bt_criar_ac(ActionEvent event) {

        if (nome_utilizador.getText() == null || passe_utilizador.getText() == null || tx_email.getText() == null || tx_contacto.getText() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            String content = "Preencha os dados todos!";
            alert.setContentText(content);
            alert.showAndWait();
        } else {
            String nome = nome_utilizador.getText();
            String palavra_passe = passe_utilizador.getText();
            String email = tx_email.getText();
            int contacto = Integer.parseInt(tx_contacto.getText());

            Funcionario funcionario = new Funcionario(nome, palavra_passe, contacto, email);
            funcionario.criarFuncionario();

        }

    }

}
